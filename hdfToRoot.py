# -*- coding: utf-8 -*-
"""
Created on Wed Jul 27 10:22:44 2022

@author: steca

Scopo di questo script e' quello di leggere tutti i .h5 in una cartella

"""
# Allows debug on spyder in my spyder-cf environment
import collections
collections.Callable = collections.abc.Callable


import os, sys
import h5py
import uproot
import glob, re



#%%
def hdfToTree(infile, outfile):
    """
    Documentazione da fare. Crea i Tree a fine spill semplicemente copiando 
    la struttura degli hdf
    """
    
    with uproot.recreate(outfile) as f:
        with h5py.File(infile, 'r') as hf:
        
            f["mytree"] = {**hf}




#%% Definisco le cartelle di origine e di fine
inpath = r".\hdf5"#sys.argv[0]
outpath = r".\proveTTree\aaa"#sys.argv[1]
# inpath = sys.argv[0]
# outpath = sys.argv[1]


# Liste di tutti i files
lstofH5 = [int(re.split(r"\\|/", f)[-1][3:9]) for f in glob.glob(os.path.join(inpath,"*.h5"))]
lstofTree = [int(re.split(r"\\|/", f)[-1][3:9]) for f in glob.glob(os.path.join(outpath,"*.ROOT"))]

print(lstofH5)
print(lstofTree)


# Ciclo su tutti i files
for i in lstofH5:
    if not i in lstofTree:
        infile = os.path.join(inpath, f"run{i}.h5")
        outfile = os.path.join(outpath, f"run{i}.ROOT")
        
        

        while(True):
            try:
                print(f"Sto per scrivere {outfile}")
                hdfToTree(infile, outfile)
                break
            except Exception as e: 
                print(e)
                print("Mannaggia, ci riproviamo tra un secondo")
                #continue
                
print("Ho finito :)")
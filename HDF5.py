# -*- coding: utf-8 -*-
"""
Created on Thu Jul 14 19:23:51 2022

@author: steca


* Ogni volta che provo ad aprire un qualsiasi file, ci metto un try, cosi, per 
evitare che il file non si possa aprire
"""

# Allows debug on spyder in my spyder-cf environment
import collections
collections.Callable = collections.abc.Callable


#%% SETTINGS
configFile = r"./config.h5.json"

# Chiavi del file json, così sono definite in un posto solo
asciiPath_key = "asciiPath"
HDF5_key = "HDF5"
numWaveform_key = "numWaveform"
currAscii_key = "currAscii"
currSpill_key = "currSpill"

Tree_key = "TreePath"


#%% LIBRARIES
import numpy as np
import os
import pandas as pd

import json
import re
import time
import sys
import glob

import h5py
from io import StringIO

import uproot




#%% FUNCTIONS

def elencaRun(asciiFolder):
    """
    RETURN: elencaRun ritorna una lista con tutti i numeri di run esistenti 
    nella cartella asciiFolder
    
    Funzionamento:
    - Mi faccio elencare tutte le prime spill
    - Splitto al separatore (os.path.sep) 
      Per compatibilità con maligno, mi tengo i due separatori a mano
    - Prendo l'ultima cosa (-1), ovvero il nome del file
    - Dal carattere 3 al 9 c'è il numero di run, converto ad intero
    
    CALL: lstofAllRun = elencaRun(settings[asciiPath_key])
    
    TODO: Magari farla in più righe, essendo una funzione, così diventa più leggibile
    
    """
    
    lstofAllRun = [int(re.split(r"\\|/", f)[-1][3:9]) for f in glob.glob(os.path.join(asciiFolder,"*_000001.dat"))]
    return lstofAllRun


def elencaSpill(asciiFolder, numRun):
    """
    RETURN: elencaSpill ritorna una lista con tutti i numeri di spill esistenti
    nella cartella asciiFolder per la run numRun
    
    CALL: lstofAllSpill = elencaSpill(settings[asciiPath_key], 501230)
    
    FEATURES:
    

    """
        
    lstofAllSpill = [int(re.split(r"\\|/", f)[-1].split("_")[-1].split(".")[0]) for f in glob.glob(os.path.join(asciiFolder,f"run{numRun}_*.dat"))]
    return lstofAllSpill





# Nota: gli ascii pariono iniziare con uno spazio: lo tolgo con lstrip
def scriviDati(HDF5file, ASCIIfile, numWF):
    """
    TODO:
        - Testare con files vuoti, non so dove potrebbe rompersi, ma credo sia
        a posto
    """
    
    # Controllo che il file non sia vuoto
    if os.path.getsize(ASCIIfile) == 0:
        print(f"Il file {ASCIIfile} è vuoto")
        return
    
            
    
    # Se ho delle waveform, apro il file ascii in questione e lo appiattisco
    with open(ASCIIfile, "r") as f:
        
        # Linee del file appiattito
        newtxt = []
        
        # Ciclo sulle linee
        for i,line in enumerate(f):
            
            if line in ["", "\n"]: continue
            
            # Se sono un multiplo di numWF+1, allora ho finito un blocco 
            if i % (numWF+1) == 0:
                # Se non sono al primo giro, mi appendo la linea precedente
                # con l'eccezione dell'ultimo spazio
                if i!=0:
                    newtxt.append(tmpLine[:-1] + "\n")
                # Replace 4, 3 and 2 spaces into 1
                tmpLine = line.lstrip().replace("\n", " ").replace("    ", " ").replace("   ", " ").replace("  ", " ")
            else:
                tmpLine = tmpLine + line.lstrip().replace("\n", " ").replace("  ", " ")
        
        # Appendo l'ultima riga
        if i>0:
            newtxt.append(tmpLine[:-1] + "\n")
        
        
        testoAppiattito = "".join(newtxt)
        csvStringIO = StringIO(testoAppiattito)
        
        # Dovrebbe essere superfluo
        if len(testoAppiattito) == 0: return
        
        df = pd.read_csv(csvStringIO, sep=" ", header=None, skiprows=0, )
            
    
    
    
    righeLastFile = len(df.index)
    print(f"Numero di colonne: {len(df.columns)}")
    print(f"Numero di righe: {righeLastFile}")

    
    # Scrivo i dati nel file HDF5
    # Posso aprirlo in append anche se non esiste
    with h5py.File(HDF5file, 'a') as hf:
    
        # Se il file non esisteva
        opts = {"compression":"gzip", "chunks":True}
        
        if (len(hf.keys())) == 0:
            
            """
            hf.create_dataset("sili", data =  df.iloc[:,0:4], maxshape=(None,4), **opts)
            hf.create_dataset("digiPH", data =  df.iloc[:,4:8], maxshape=(None,4), **opts)
            hf.create_dataset("digiTime", data =  df.iloc[:,8:12], maxshape=(None,4), **opts)
            hf.create_dataset("time_run", data =  df.iloc[:,12], maxshape=(None,), **opts)
            hf.create_dataset("time_abs", data =  df.iloc[:,13], maxshape=(None,), **opts)
            hf.create_dataset("nev", data =  df.iloc[:,14],  maxshape=(None,), **opts)
            
            for i in range(numWF):
                hf.create_dataset(f"wf{i}", data =  df.iloc[:,(15+1024*i):(15+1024*(i+1))], maxshape=(None,1024), **opts)
            """
            
            # H2 Settings
            hf.create_dataset("xpos", data =  df.iloc[:,0:4], maxshape=(None,4), **opts)
            hf.create_dataset("nstrip", data =  df.iloc[:,4:8], maxshape=(None,4), **opts)
            hf.create_dataset("nclu", data =  df.iloc[:,8:12], maxshape=(None,4), **opts)
            hf.create_dataset("digi_base", data =  df.iloc[:,12:28], maxshape=(None,16), **opts)
            hf.create_dataset("digi_ph", data =  df.iloc[:,28:44], maxshape=(None,16), **opts)
            hf.create_dataset("digi_time", data =  df.iloc[:,44:60], maxshape=(None,16), **opts)
            hf.create_dataset("xinfo", data =  df.iloc[:,60:65], maxshape=(None,5), **opts)
            hf.create_dataset("info_plus", data =  df.iloc[:,65:67], maxshape=(None,2), **opts)
            hf.create_dataset("Ievent", data =  df.iloc[:,67],  maxshape=(None,), **opts)

            
        else:
            # existingRows = hf["sili"].shape[0] #not implemented
            
            for k in hf.keys():
                hf[k].resize((hf[k].shape[0] + righeLastFile), axis = 0)
                
               
            """
            hf["sili"][-righeLastFile:] = df.iloc[:,0:4]
            hf["digiPH"][-righeLastFile:] = df.iloc[:,4:8]
            hf["digiTime"][-righeLastFile:] = df.iloc[:,8:12]
            hf["time_run"][-righeLastFile:] = df.iloc[:,12]
            hf["time_abs"][-righeLastFile:] = df.iloc[:,13]
            hf["nev"][-righeLastFile:] = df.iloc[:,14]
            
            for i in range(numWF):
                hf[f"wf{i}"][-righeLastFile:] = df.iloc[:,(15+1024*i):(15+1024*(i+1))]
            """
            
            # H2 Settings
            hf["xpos"][-righeLastFile:] = df.iloc[:,0:4]
            hf["nstrip"][-righeLastFile:] = df.iloc[:,4:8]
            hf["nclu"][-righeLastFile:] = df.iloc[:,8:12]
            hf["digi_base"][-righeLastFile:] = df.iloc[:,12:28]
            hf["digi_ph"][-righeLastFile:] = df.iloc[:,28:44]
            hf["digi_time"][-righeLastFile:] = df.iloc[:,44:60]
            hf["xinfo"][-righeLastFile:] = df.iloc[:,60:65]
            hf["info_plus"][-righeLastFile:] = df.iloc[:,65:67]
            hf["Ievent"][-righeLastFile:] = df.iloc[:,67]

            
    return #newtxt




def aggiornaConfig(settings):
    """
    aggiornaConfig permette di aggiornare il dizionario delle configurazioni.
    Chiamare passando il dizionario stesso. Il file viene invece automaticamente
    pescato dalla funzione.
    
    CALL: aggiornaConfig(settings)
    """
    # Lo salvo
    try:
        with open(configFile, "w") as f:
            json.dump(settings, f, indent=4)
    except Exception as e: 
        print(e)
        print("Non ho salvato le informazioni nel dizionario, resteranno solo in memoria")
        
    return





def hdfToTree(infile, outfile):
    """
    Documentazione da fare. Crea i Tree a fine spill semplicemente copiando 
    la struttura degli hdf
    """
    
    with uproot.recreate(outfile) as f:
        with h5py.File(infile, 'r') as hf:
        
            f["mytree"] = {**hf}
    

            




#%% PRELIMINARY CHECK + load config

# Controllo che esista il file di config
try:
    with open(configFile, "r") as f:
        settings = json.load(f)
except:
    print("Sicuro che esista il file di config?")
    sys.exit(1)
    
    
# Check if folder exist
if not os.path.isdir(settings[asciiPath_key]):
    print(f"Ascii path not valid\t{settings[asciiPath_key]}")
    sys.exit(1)
    
if not os.path.isdir(settings[HDF5_key]):
    print(f"HDF5 path not valid\t{settings[HDF5_key]}")
    sys.exit(1)
    
    
if not os.path.isdir(settings[Tree_key]):
    print(f"HDF5 path not valid\t{settings[Tree_key]}")
    sys.exit(1)
    
    
    
#%% Big loop

while(True):
    
    # Ottengo la lista di tutte le run esistenti
    lstofAllRun = elencaRun(settings[asciiPath_key])
    
    # Controllo che esista nella directory almeno una run, per consistenza
    # con i pezzi seguenti
    if len(lstofAllRun) == 0:
        continue
        # wait till there exist at least one ascii file


    # Ottengo la lista delle spill dell'ASCII corrente
    lstofAllSpill = elencaSpill(settings[asciiPath_key], settings[currAscii_key])
    
    
    # Se inizializzo bene non dovrebbe essere vitale, ma costa poca fatica lasciarlo
    # e poi è più robusto
    
    # Se l'ASCII non esiste (e quindi non ha spill, verosimilmente è un pede),
    # passo all'ASCII dopo
    if len(lstofAllSpill) == 0: 
        settings[currAscii_key] += 1
        settings[currSpill_key] = 0
        aggiornaConfig(settings)
        continue
    
    
    # Se esiste una nuova spill dell'ASCII corrente
    # La scrivo ed incremento il contatore delle spill
    # Se ho un errore in scrittura, semplicemente passo oltre
    if np.array(lstofAllSpill).max() > settings[currSpill_key]:
        try:
            infile = os.path.join(settings[asciiPath_key], f"run{settings[currAscii_key]}_{settings[currSpill_key]+1:06d}.dat")
            outfile = os.path.join(settings[HDF5_key], f"run{settings[currAscii_key]}.h5")
            
            print(f"Sto per appendere {infile} a {outfile}")
            scriviDati(outfile, infile, settings[numWaveform_key])
            
            settings[currSpill_key] += 1
            aggiornaConfig(settings)
        except Exception as e: 
            print(e)
            print("Mannaggia, ci riproviamo al prossimo giro")
            #continue
            
        
    # Se non ho nuove spill, ma in compenso esiste un nuovo ascii        
    elif np.array(lstofAllRun).max() > settings[currAscii_key]:
        
        # NOTA PER IL ME DEL FUTURO:
        # In questo preciso punto ci si arriva solo quando ho finito di appendere
        # i dati dell'ultima spill e mi accorgo che esiste la run successiva.
        # è il punto ideale per creare in un colpo solo tree, npz o formati 
        # compressi non appendibili
        # settings[currAscii_key] è il numero della run che mi interessa
        
        
        
        # Prova da debuggare. 
        # Non l'ho implementato: se qualcuno vuole c'è un file che converte
        # tutto in massa
        """
        infile = os.path.join(settings[HDF5_key], f"run{settings[currAscii_key]}.h5")
        outfile = os.path.join(settings[Tree_key], f"run{settings[currAscii_key]}.ROOT")
        
        print(f"Sto per scrivere {infile} in {outfile}")

        while(True):
            try:
                hdfToTree(infile, outfile)
                break
            except Exception as e: 
                print(e)
                print("Mannaggia, IN REALTA' QUESTA ECCEZIONE (forse) NON E' GESTITA")
                #continue
        """
        # Fine prova
        
        
        
        
        
        # Incremento il contatore
        settings[currAscii_key] += 1
        
        # Incremento finche non esiste la run
        while not(settings[currAscii_key] in lstofAllRun):
            settings[currAscii_key] += 1

        # Imposto la spill a 0 ed incremento il contatore
        settings[currSpill_key] = 0
        aggiornaConfig(settings)
        
        
        


    time.sleep(5)




#%% DEPOSITO
"""
    
#%% Debug >> console
a = scriviDati("ZZZ.h5", r".\proveHDF5\fakeAsciiHDF5\run360263.compact", 4)
hf = h5py.File('ZZZ.h5', 'a')
hf = h5py.File('HDF5/run360263.h5', 'a')
hf["sili"].shape
hf.close()


#% DEBUG
# Usato per stamparmi un po' di file

# file di PMG con 4 wf
# with open("./proveHDF5/fakeAsciiHDF5/run360257.compact", "r") as f:
with open("./proveHDF5/fakeAsciiHDF5/run360263.compact", "r") as f:
    for i, line in enumerate(f):
        if i<2:
            print(i)
            print(line.strip())
            # print(len(line.split(" ")))
            # print(line.split(" "))


#% DEBUG
# Righe usate per crearmi alcuni files da 100 righe

# file di PMG con 4 wf
# with open("./proveHDF5/fakeAsciiHDF5/run360257.compact", "r") as f:
with open("./proveHDF5/fakeAsciiHDF5/run360263.compact", "r") as f:
    with open("./proveHDF5/fakeAsciiPMG/run360263_000001.dat", "w") as of:
        for i, line in enumerate(f):
            if (i>=00) & (i<100):
                of.write(line)

"""
import serial
from serial import *

# Definisco la porta seriale e le impostazioni
ser = serial.Serial("/dev/ttyUSB0", baudrate = 115200, bytesize = EIGHTBITS, stopbits = STOPBITS_ONE, parity = PARITY_NONE, timeout = 5)

# Apro la porta se non gia' aperta
if not ser.isOpen():
        ser.open()

# Entriamo in modalita' macchina
ser.write("AT+MACHINE\r\n".encode())

# Imposto la tensione
voltage = 30.0
print(f"Sto impostando la tensione a " + str(voltage) + "V")
strToWrite = "AT+SET,2," + str(voltage) + "\r\n"
ser.write(strToWrite.encode())
ser.write("AT+GET,2\r\n".encode())
tmp = ser.read(100).decode()
print("La tensione che mi dice lui vale " + str(tmp))

ser.write("AT+GET,231\r\n".encode())
tmp = ser.read(100).decode()
print("La tensione ADC readback" + str(tmp))


# Accendo
print(f"Sto per abilitare l'uscita")
ser.write("AT+SET,0,1\r\n".encode())
ser.write("AT+GET,0\r\n".encode())
tmp = ser.read(100).decode()
print("Risposta " + str(tmp))



ser.write("AT+GET,231\r\n".encode())
tmp = ser.read(100).decode()
print("La tensione ADC readback" + str(tmp))



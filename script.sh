#!/bin/bash
sourcedir="./ascii_daq_sshfs"
targetdir="/eos/user/s/scarsi/testBeam/TB_H2_2022/ASCII_MICHELA"

echo "I will synchronize " $sourcedir " with " $targetdir

if [ ! -d $sourcedir ]; then
	echo $sourcedir " does not exists"
	exit 1
fi

if [ ! -d $targetdir ]; then
	echo $targetdir " does not exists"
	exit 1
fi




sleepseconds=10

while true; do
	rsync -avrz $sourcedir $targetdir
	echo "I will sleep " $sleepseconds " seconds"
	sleep $sleepseconds
done


# What's here
Files che vengono copiati sul PC dell'acquisizione a cui è stato attaccato il gechino
Dopo avergli abilitato la scrittura, con questi tre pitoni si può
- [accendi.py](accendi.py): Accendere (la tensione va impostata da dentro il file alla riga `voltage`
- [spegni.py](spegni.py): Spegnere il bias
- [status.py](status.py): Printa tensione e corrente riletti (non i valori impostati)

Dalla directory in cui sono, eseguire
```bash
python3 script.py
```
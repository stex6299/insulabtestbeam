<div style=\"background-color:red;color:yellow;padding:2%;font-size: 50px;font-weight: bold;text-align: center;\">THIS REPO IS NO LONGER SUPPORTED</div>
We moved to [the new one](https://gitlab.com/stex6299/insulabtestbeamV2)

# Insulab test beam tools - Will be used in July-August 2022
The file [HDF5.py](HDF5.py) allows you to process and store ASCII files as they arrives, without waiting for the ending of the spill. Everyone can read these files in real time.

Inside the function `scriviDati`, you have to define the scheme of the data: in fact it's not store just a big matrix, but data are grouped. This way, we can avoid to load the waveforms. In future I plan to create a way to config outside the python.

In the [config.h5.json](config.json) file you can set the directories for ASCII (usually mounted via sshfs), for the HDF5 files, for the amount of waveforms and to keep trace to the last processed file, even in case of crashes.

Before loading a file, there is a `try ... except` block, in order not to crash. If it can not access a file, it simple retry in the next iteration.

Based on a known bug from a previous version, now files are stored in cronological order (i.e. spill1, spill2, spill3...)

## Config.json
The [configuration file](config.h5.json) is a simply json string where you can define:
- The path in which ascii files are `asciiPath`
- The path of where HDF5 will be put `HDF5`
- Number of digitized waveforms `numWaveform`
- Current run which is being processed `currAscii`
- Last processed spill of the `currAscii` run `currSpill`
- The path in which Root files will be saved `TreePath`. Actually not implemented


## Example of reading such a data
See an example [here](https://scarsi.web.cern.ch/MISC/Python/proveH5.html). Shortly:
```python3
import h5py

with h5py.File('data.h5', 'r') as hf:
	print(hf.keys())
	hf["sili"].shape
```

## Support tools
The file [hdfToRoot.py](hdfToRoot.py) allows you to translate every HDF5 file into a ROOT TTree file, by simply providing two directoriesù
```bash
$ python3 hdfToRoot.py /path/of/HDF5 /path/of/TTree
```











# Insulab test beam tools - Used @T9, June 2022
The file [mainScript.py](mainScript.py) allows you to compact files from multiple ASCII produced during a test beam (when you have a file per spill) and to produce npz from compact ASCII
In the [config.json](config.json) file you can set the directories for ascii (usually mounted via sshfs), compact and npz. The same file is used by the script in order to have memory of the last processed file even if crashes


## Config.json
The [configuration file](config.json) is a simply json string where you can define:
- The path in which ascii files are (asciiPath)
- The path of where compact ascii will be put (compactPath)
- The path of where npy files will be put (npzPath)
- How many row of waveforms are present (skipRows)
- An indicator of the next file to try to compact if exists (lastCompactedAscii)
- An indicator of the next file to try to npy if exists (lastNumpyzedCompact)


## Mount sshfs
On my vm, in the directory ` /home/scarsi-home/TBgiugno2022/insulabtestbeam/asciiSshfs` mounted via 
```bash
sshfs -o ro insudaq@128.141.115.149:/data/insudaq/ascii_cern2022_t9 .
```



## It's a bug... no! It's a feature!
Fino a quando non esiste la run $n+1$-esima, la run $n$-esima non viene processata. Un trick consiste nel copiare il primo ascii dell'ultima run per esempio
```bash
cp run500900_000001.dat run500901_000001.dat
```
questo sblocca il processing dell'ultima run. Un'altra idea consiste nel far generare all'acquisizione una run a caso. 
Nota: per i pede non viene prodotto l'ascii
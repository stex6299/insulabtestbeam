# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 21:01:39 2022

@author: steca
"""

import numpy as np

import glob

# =============================================================================
# lst = glob.glob(r".\ascii_seldom\*600127*.dat")
# 
# print(f"loading {lst[0]}")
# dataMatrix = np.loadtxt(lst[0])
# 
# for i, file in enumerate(lst):
#     if i==0: continue
# 
#     print(f"loading {file}")
#     
#     # Leggo il file
#     tmpMat = np.loadtxt(file)
#     
#     # Lo appendo alla matriciona
#     dataMatrix = np.vstack((dataMatrix.copy(), tmpMat.copy()))
# =============================================================================
    
    
    
#%%
def loadAsciiSpill(numRun):
    lst = glob.glob(rf".\ascii_seldom\*{numRun}*.dat")

    print(f"loading {lst[0]}")
    dataMatrix = np.loadtxt(lst[0])

    for i, file in enumerate(lst):
        if i==0: continue

        print(f"loading {file}")
        
        # Leggo il file
        tmpMat = np.loadtxt(file)
        
        # Lo appendo alla matriciona
        dataMatrix = np.vstack((dataMatrix.copy(), tmpMat.copy()))
        
        
    return dataMatrix

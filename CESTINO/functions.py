# -*- coding: utf-8 -*-
"""
Created on Sun Jun 19 10:17:58 2022

@author: steca
"""

import numpy as np
import os
import re


# Legge i settings dal file di configurazione
def getSettings(file):
    myDict = {}
    
    with open(file, "r") as f:
        for line in f:
            # Ignore comments
            if line.strip().startswith("#"): continue
        
            mySplit = line.split("=")
            
            if len(mySplit) != 2: continue
        
            myDict[mySplit[0].strip()] = mySplit[1].strip()
            
            
    return myDict
        
        
#%%

def compattaAscii(lstOfFiles, skiprows, compactPath):
    
    outFile = os.path.join(compactPath, "run"+re.split(r'\\|/', lstOfFiles[0])[-1][3:9]+".compact")
    print(f"Sto per scrivere {outFile}")
    
    with open(outFile, "w") as outf:
        
        for inFile in lstOfFiles:
            print(f"Reading {inFile}")
            with open(inFile, "r") as inf:
                
                for i,line in enumerate(inf):
                    if skiprows == 0:                        
                        outf.write(line)
                    elif i % skiprows == 0:
                        outf.write(line)

                        
    print(f"Ho scritto {outFile}")
    
    
    
def prepareNpz(compactFile, npzPath):
    
    outFile = os.path.join(npzPath, "run"+re.split(r'\\|/', compactFile)[-1][3:9]+".npz")
    print(f"Sto per scrivere {outFile}")

    
    myArr = np.loadtxt(compactFile)
    np.save(outFile, myArr)
    print(f"Ho scritto {outFile}")